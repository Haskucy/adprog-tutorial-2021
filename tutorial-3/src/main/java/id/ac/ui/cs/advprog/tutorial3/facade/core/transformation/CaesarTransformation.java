package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation implements Transformation{
    private int key;

    public CaesarTransformation(int key){
        this.key = key;
    }

    public CaesarTransformation(){
        this(2);
    }


    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? key % 26 + 26: (26 - key) % 26 + 26;
        int n = text.length();
        char[] result = new char[n];
        for (int i = 0; i < n; i++) {
            if (Character.isLetter(text.charAt(i))) {
                if (Character.isUpperCase(text.charAt(i))) {
                    char thisChar = (char)(((int)text.charAt(i) + selector - 65) % 26 + 65);
                    result[i] = thisChar;
                } else {
                    char thisChar = (char)(((int)text.charAt(i) + selector - 97) % 26 + 97);
                    result[i] = thisChar;
                }
            } else {
                result[i] = text.charAt(i);
            }
        }
        return new Spell(new String(result), codex);


    }



}
