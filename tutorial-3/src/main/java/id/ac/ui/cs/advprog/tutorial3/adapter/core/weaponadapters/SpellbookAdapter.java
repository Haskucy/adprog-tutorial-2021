package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;


// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean recharging;

    public SpellbookAdapter(Spellbook spellbook) {
        this.recharging = false;
        this.spellbook = spellbook;
    }

    @Override
    public String normalAttack() {
        this.recharging = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!this.recharging){
            this.recharging = true;
            return spellbook.largeSpell();
        }
        return "";
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

    public boolean isRecharging() {
        return this.recharging;
    }
}
