package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public interface Transformation {
    public Spell encode(Spell spell);
    public Spell decode(Spell spell);

}
