package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;

import java.util.ArrayList;
import java.util.List;

public class FacadeTranslate {
    List<Transformation> listTransformation;
    Transformation celestialTransformation;
    Transformation abyssalTransformation;
    Transformation caesarTransformation;

    public FacadeTranslate() {
        this.celestialTransformation = new CelestialTransformation();
        this.abyssalTransformation = new AbyssalTransformation();
        this.caesarTransformation = new CaesarTransformation();
        this.listTransformation = new ArrayList<>();

        listTransformation.add(celestialTransformation);
        listTransformation.add(abyssalTransformation);
        listTransformation.add(caesarTransformation);
    }

    public Spell encode(Spell spell) {
        for (Transformation transformation : listTransformation) {
            spell = transformation.encode(spell);
        }
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
        return spell;
    }

    public Spell decode(Spell spell) {
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        for (int i = listTransformation.size() - 1; i >= 0; i--) {
            spell = listTransformation.get(i).decode(spell);
        }
        return spell;
    }

}
