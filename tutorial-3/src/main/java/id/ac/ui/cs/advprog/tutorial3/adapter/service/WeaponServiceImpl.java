package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~ DE
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)
    private static boolean init = true;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;


    @Autowired
    private LogRepository logRepository;


    // TODO: implement me DE
    @Override
    public List<Weapon> findAll() {

        if(init){
            for (Bow bow : bowRepository.findAll()) {
                weaponRepository.save(new BowAdapter(bow));
            }

            for (Spellbook spellbook : spellbookRepository.findAll()){
                weaponRepository.save(new SpellbookAdapter(spellbook));
            }
            init = false;
        }


        return weaponRepository.findAll();


    }

    // TODO: implement me DE
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        boolean realized = true;

        if (weaponName.equals("Heat Bearer") || weaponName.equals("The Windjedi")){
            SpellbookAdapter spellbook = (SpellbookAdapter) weapon;
            if (attackType == 2 && spellbook.isRecharging()){
                realized = false;
            }

        }
        String attackTypeStr = "";
        String attackDesc = "";

        if (attackType == 1){
            attackTypeStr = "normal attack";
            attackDesc = weapon.normalAttack();
        }
        else if (attackType == 2){
            attackTypeStr = "charged attack";
            attackDesc = weapon.chargedAttack();
        }

        if(realized){
            logRepository.addLog(String.format("%s attacked with %s (%s): %s", weapon.getHolderName(), weapon.getName(), attackTypeStr, attackDesc));
        }



    }

    // TODO: implement me DE
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
