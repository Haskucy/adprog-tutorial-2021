package id.ac.ui.cs.advprog.tutorial2.template.core;

public class Archer extends SpiritInQuest {

    @Override
    protected String buff() {
        return "Buff Buster Attack with 100% Damage";
    }

    @Override
    protected String attack() {
        return "Attack with Buster + 100% Damage";
    }

    @Override
    protected String attackWithSpecialSkill() {
        return "Duaaarrr Duaaarrr";
    }

}