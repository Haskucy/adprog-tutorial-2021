package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

public interface Spell {
    void cast();
    void undo();
    String spellName();
}
