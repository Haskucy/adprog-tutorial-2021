package id.ac.ui.cs.advprog.tutorial2.template.core;

public class Lancer extends SpiritInQuest {

    @Override
    protected String buff() {
        return "Buff Arts Attack with 100% Damage";
    }

    @Override
    protected String attack() {
        return "Attack with Arts + 100% Damage";
    }

    @Override
    protected String attackWithSpecialSkill() {
        return "Whooosssh Whooosssh";
    }

}