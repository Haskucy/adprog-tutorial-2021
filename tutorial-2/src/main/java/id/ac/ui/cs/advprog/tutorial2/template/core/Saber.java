package id.ac.ui.cs.advprog.tutorial2.template.core;

public class Saber extends SpiritInQuest {

    @Override
    protected String buff() {
        return "Buff Quick Attack with 100% Damage";
    }

    @Override
    protected String attack() {
        return "Attack with Quick + 100% Damage";
    }


    @Override
    protected String attackWithSpecialSkill() {
        return "Sreeettt Sreeettt";
    }

}