package csui.advpro2021.tais.service;

import csui.advpro2021.tais.core.BayarLog;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl implements LogService{
    @Autowired
    private LogRepository logRepository;
    @Autowired
    private MahasiswaRepository mahasiswaRepository;


    @Override
    public Log createLog(String npm, Log log) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if(mahasiswa == null || mahasiswa.getMatkulAsisten() == null){
            return null;
        }

        log.setAsisten(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log updateLog(int idLog, Log log) {
       Log logLama = logRepository.findByIdLog((idLog));
       if(logLama == null){
           return null;
       }
        log.setAsisten(logLama.getAsisten());
        log.setIdLog(idLog);
        logRepository.save(log);
        return log;

    }

    @Override
    public void deleteLogByIdLog(int idLog) {
        if(logRepository.findByIdLog(idLog)==null){
            return;
        }
        logRepository.delete(logRepository.findByIdLog(idLog));

    }

    @Override
    public Iterable<Log> getListLog(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if(mahasiswa == null || mahasiswa.getMatkulAsisten() == null){
            return null;
        }
        return mahasiswa.getLogAsisten();

    }

    @Override
    public List<BayarLog> bikinGajian(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if (mahasiswa != null) {
            List<Log> assistantLogs = mahasiswaRepository.findByNpm(npm).getLogAsisten();
            return BayarLog.bikinListBayaranLog(assistantLogs);
        }
        return null;

    }
}
