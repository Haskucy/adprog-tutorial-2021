package csui.advpro2021.tais.service;

import csui.advpro2021.tais.core.BayarLog;
import csui.advpro2021.tais.model.Log;

import java.util.List;

public interface LogService {
    Log createLog(String npm, Log log);
    Log updateLog(int idLog, Log log);
    void deleteLogByIdLog(int idLog);

    Iterable<Log> getListLog(String npm);
    List<BayarLog> bikinGajian(String npm);

}
