package csui.advpro2021.tais.core;

import csui.advpro2021.tais.model.Log;
import lombok.Data;
import lombok.Setter;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Setter
public class BayarLog {
    private String bulan;
    private int jamKerja;
    private int bayar;

    public static List<BayarLog> bikinListBayaranLog(List<Log> logList){

        List<BayarLog> listBayarLog = new ArrayList<BayarLog>();

        for(int bulan = 0; bulan < 12; bulan++){

            int finalBulan = bulan;
            List<Log> logSetiapBulannya = logList.stream().filter(log -> log.getStartLog().getMonth() == finalBulan).collect(Collectors.toList());

            int jam = 0;
            for (Log log : logSetiapBulannya){
                jam += (log.getEndLog().getTime() - log.getStartLog().getTime()) / (60 * 60 * 1000);
            }
            if (jam != 0) {
                BayarLog bayarLog = new BayarLog();
                bayarLog.setJamKerja(jam);
                bayarLog.setBayar(350 * jam);
                bayarLog.setBulan(new DateFormatSymbols().getMonths()[bulan]);

                listBayarLog.add(bayarLog);
            }
        }
        return listBayarLog;
    }

}
