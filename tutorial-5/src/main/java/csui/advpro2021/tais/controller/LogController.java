package csui.advpro2021.tais.controller;


import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {

    @Autowired
    LogService logService;

    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLog(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getListLog(npm));
    }

    @PostMapping(path = "/{npm}", produces = {"application/json"}, consumes = {"application/json"})
    @ResponseBody
    public ResponseEntity postLog(@RequestBody Log log, @PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.createLog(npm, log));
    }

    @PutMapping(path = "/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "idLog") int idLog, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(idLog, log));
    }

    @GetMapping(path = "/{npm}/log_report", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getPayLog(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.bikinGajian(npm));
    }
}
