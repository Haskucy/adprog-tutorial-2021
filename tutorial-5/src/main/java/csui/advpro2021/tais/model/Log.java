package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @Column(name = "id_log", updatable = false)
    private int idLog;

    @Column(name = "start_log")
    private Timestamp startLog;

    @Column(name = "end_log")
    private Timestamp endLog;

    @Column(name = "deskripsi")
    private String deskripsi;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "asisten", nullable = false)
    private Mahasiswa asisten;
}


