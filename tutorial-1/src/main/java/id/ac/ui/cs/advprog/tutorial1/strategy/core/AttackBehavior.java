package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public interface AttackBehavior extends Strategy {

    String attack();
    public String getType();
}
