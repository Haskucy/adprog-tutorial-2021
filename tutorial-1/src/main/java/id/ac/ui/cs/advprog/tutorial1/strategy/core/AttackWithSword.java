package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {

    //public void AttackWithSword(){}

    @Override
    public String attack() {
        return "lets do this, aibo";
    }

    @Override
    public String getType() {
        return "Sword";
    }
}
