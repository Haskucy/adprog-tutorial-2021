package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {

    @Override
    public String defend() {
        return "you can make barrier by crossing your finger";
    }

    @Override
    public String getType() {
        return "Barrier";
    }
}
