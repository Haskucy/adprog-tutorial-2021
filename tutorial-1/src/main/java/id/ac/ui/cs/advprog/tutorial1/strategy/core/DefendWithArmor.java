package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {

    @Override
    public String defend() {
        return "My armor is impenetrable";
    }

    @Override
    public String getType() {
        return "Armor";
    }
}
