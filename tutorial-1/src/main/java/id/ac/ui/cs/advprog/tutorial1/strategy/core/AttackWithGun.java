package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    // public void AttackWithGun(){}

    @Override
    public String attack() {
        return "ITS HIGH NOON";
    }

    @Override
    public String getType() {
        return "Gun";
    }
}
