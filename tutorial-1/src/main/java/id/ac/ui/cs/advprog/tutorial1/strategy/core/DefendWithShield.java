package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {

    @Override
    public String defend() {
        return "I am not shield hero, but i raising my shield";
    }

    @Override
    public String getType() {
        return "Sheild";
    }
}
