package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {

    //public void AttackWithMagic(){}

    @Override
    public String attack() {
        return "My name is Mystic! I am a user of the finest magic crimson demons possess, and I commend explosion magic! Behold my power! EXPLOSION!";
    }

    @Override
    public String getType() {
        return "Magic";
    }
}
