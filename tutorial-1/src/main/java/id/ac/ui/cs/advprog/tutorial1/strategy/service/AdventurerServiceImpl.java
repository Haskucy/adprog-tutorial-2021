package id.ac.ui.cs.advprog.tutorial1.strategy.service;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.stereotype.Service;

@Service
public class AdventurerServiceImpl implements AdventurerService {

    private final AdventurerRepository adventurerRepository;
    private final StrategyRepository strategyRepository;


    public AdventurerServiceImpl(
            AdventurerRepository adventurerRepository,
            StrategyRepository strategyRepository) {

        this.adventurerRepository = adventurerRepository;
        this.strategyRepository = strategyRepository;
    }

    @Override
    public Iterable<Adventurer> findAll() {
        return adventurerRepository.findAll();
    }

    @Override
    public Adventurer findByAlias(String alias) {
        return adventurerRepository.findByAlias(alias);
    }

    @Override
    public void changeStrategy(String alias, String attackType, String defenseType) {
        Adventurer adventurer = findByAlias(alias);
        AttackBehavior attackBehavior;
        DefenseBehavior defenseBehavior;

        switch(attackType) {
            case "Gun":
               attackBehavior = new AttackWithGun();
                break;
            case "Magic":
                attackBehavior = new AttackWithMagic();
                break;
            case "Sword":
                attackBehavior = new AttackWithSword();
                break;
            default:
                attackBehavior = adventurer.getAttackBehavior();
        }

        switch(defenseType) {
            case "Armor":
                defenseBehavior = new DefendWithArmor();
                break;
            case "Shield":
                defenseBehavior = new DefendWithShield();
                break;
            case "Barrier":
                defenseBehavior = new DefendWithBarrier();
                break;
            default:
                defenseBehavior = adventurer.getDefenseBehavior();
        }
        adventurer.setAttackBehavior(attackBehavior);
        adventurer.setDefenseBehavior(defenseBehavior);
    }

    @Override
    public Iterable<AttackBehavior> getAttackBehaviors() {
        return strategyRepository.getAttackBehaviors();
    }

    @Override
    public Iterable<DefenseBehavior> getDefenseBehaviors() {
        return strategyRepository.getDefenseBehaviors();
    }
}
