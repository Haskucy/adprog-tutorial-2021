package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonTest {
    private MondoUdon mondoUdon;

    @BeforeEach
    public void setup() throws Exception{
        mondoUdon = new MondoUdon("udon mondo");
    }

    @Test
    public void getNameTest() throws Exception{
        assertEquals("udon mondo",mondoUdon.getName());
    }

    @Test
    public void getNoodleTest() throws Exception{
        assertTrue((mondoUdon.getNoodle() instanceof Udon));
    }

    @Test
    public void getMeatTest() throws Exception{
        assertTrue((mondoUdon.getMeat() instanceof Chicken));
    }

    @Test
    public void getToppingTest() throws Exception{
        assertTrue((mondoUdon.getTopping() instanceof Cheese));
    }

    @Test
    public void getFlavorTest() throws Exception{
        assertTrue((mondoUdon.getFlavor() instanceof Salty));
    }
}
