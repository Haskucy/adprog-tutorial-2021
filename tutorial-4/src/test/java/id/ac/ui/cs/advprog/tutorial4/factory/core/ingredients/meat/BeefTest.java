package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BeefTest {
    private Class<?> classBeef;
    private Meat beef;

    @BeforeEach
    public void setup() throws Exception {
        classBeef = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef");
        beef = new Beef();
    }

    @Test
    public void publicConcreteBeefTest() throws Exception {
        int classModifiers = classBeef.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
        assertFalse((Modifier.isInterface(classModifiers)));
    }

    @Test
    public void publicConcreteGetDescriptionTest() throws Exception {
        Method getDescription = classBeef.getDeclaredMethod("getDescription");
        int getDescriptionModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(getDescriptionModifiers));
        assertFalse(Modifier.isAbstract(getDescriptionModifiers));
    }

    @Test
    public void publicContentsGetDescriptionTest() throws Exception {
        assertEquals("Adding Maro Beef Meat...", beef.getDescription());
    }

    @Test
    public void BeefIsMeatTest() throws Exception {
        assertTrue(Meat.class.isInstance(beef));
    }
}
