package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FishTest {
    private Class<?> classFish;
    private Meat fish;

    @BeforeEach
    public void setup() throws Exception {
        classFish = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish");
        fish = new Fish();
    }

    @Test
    public void publicConcreteFishTest() throws Exception {
        int classModifiers = classFish.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
        assertFalse((Modifier.isInterface(classModifiers)));
    }

    @Test
    public void publicConcreteGetDescriptionTest() throws Exception {
        Method getDescription = classFish.getDeclaredMethod("getDescription");
        int getDescriptionModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(getDescriptionModifiers));
        assertFalse(Modifier.isAbstract(getDescriptionModifiers));
    }

    @Test
    public void publicContentsGetDescriptionTest() throws Exception {
        assertEquals("Adding Zhangyun Salmon Fish Meat...", fish.getDescription());
    }

    @Test
    public void FishIsMeatTest() throws Exception {
        assertTrue(Meat.class.isInstance(fish));
    }
}

