package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SweetTest {
    private Class<?> classSweet;
    private Flavor sweet;

    @BeforeEach
    public void setup() throws Exception {
        classSweet = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet");
        sweet = new Sweet();
    }

    @Test
    public void publicConcreteSweetTest() throws Exception {
        int classModifiers = classSweet.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
        assertFalse((Modifier.isInterface(classModifiers)));
    }

    @Test
    public void publicConcreteGetDescriptionTest() throws Exception {
        Method getDescription = classSweet.getDeclaredMethod("getDescription");
        int getDescriptionModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(getDescriptionModifiers));
        assertFalse(Modifier.isAbstract(getDescriptionModifiers));
    }

    @Test
    public void publicContentsGetDescriptionTest() throws Exception {
        assertEquals("Adding a dash of Sweet Soy Sauce...", sweet.getDescription());
    }

    @Test
    public void SweetisFlavorTest() throws Exception {
        assertTrue(Flavor.class.isInstance(sweet));
    }
}
