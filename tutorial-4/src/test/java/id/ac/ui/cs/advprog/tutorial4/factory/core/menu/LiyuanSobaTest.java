package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaTest {
    private LiyuanSoba liyuanSoba;

    @BeforeEach
    public void setup() throws Exception{
        liyuanSoba = new LiyuanSoba("soba liyuan");
    }

    @Test
    public void getNameTest() throws Exception{
        assertEquals("soba liyuan",liyuanSoba.getName());
    }

    @Test
    public void getNoodleTest() throws Exception{
        assertTrue((liyuanSoba.getNoodle() instanceof Soba));
    }

    @Test
    public void getMeatTest() throws Exception{
        assertTrue((liyuanSoba.getMeat() instanceof Beef));
    }

    @Test
    public void getToppingTest() throws Exception{
        assertTrue((liyuanSoba.getTopping() instanceof Mushroom));
    }

    @Test
    public void getFlavorTest() throws Exception{
        assertTrue((liyuanSoba.getFlavor() instanceof Sweet));
    }
}
