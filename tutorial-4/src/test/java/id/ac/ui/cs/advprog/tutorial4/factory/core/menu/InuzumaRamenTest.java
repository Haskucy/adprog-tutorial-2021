package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenTest {
    private InuzumaRamen inuzumaRamen;

    @BeforeEach
    public void setup() throws Exception{
        inuzumaRamen = new InuzumaRamen("ramen inuzuma");
    }

    @Test
    public void getNameTest() throws Exception{
        assertEquals("ramen inuzuma",inuzumaRamen.getName());
    }

    @Test
    public void getNoodleTest() throws Exception{
        assertTrue((inuzumaRamen.getNoodle() instanceof Ramen));
    }

    @Test
    public void getMeatTest() throws Exception{
        assertTrue((inuzumaRamen.getMeat() instanceof Pork));
    }

    @Test
    public void getToppingTest() throws Exception{
        assertTrue((inuzumaRamen.getTopping() instanceof BoiledEgg));
    }

    @Test
    public void getFlavorTest() throws Exception{
        assertTrue((inuzumaRamen.getFlavor() instanceof Spicy));
    }
}
