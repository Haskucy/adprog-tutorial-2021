package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BoiledEggTest {
    private Class<?> classBoiledEgg;
    private Topping boiledEgg;

    @BeforeEach
    public void setup() throws Exception {
        classBoiledEgg = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg");
        boiledEgg = new BoiledEgg();
    }

    @Test
    public void publicConcreteBoiledEggTest() throws Exception {
        int classModifiers = classBoiledEgg.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
        assertFalse((Modifier.isInterface(classModifiers)));
    }

    @Test
    public void publicConcreteGetDescriptionTest() throws Exception {
        Method getDescription = classBoiledEgg.getDeclaredMethod("getDescription");
        int getDescriptionModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(getDescriptionModifiers));
        assertFalse(Modifier.isAbstract(getDescriptionModifiers));
    }

    @Test
    public void publicContentsGetDescriptionTest() throws Exception {
        assertEquals("Adding Guahuan Boiled Egg Topping", boiledEgg.getDescription());
    }

    @Test
    public void BoiledEggisToppingTest() throws Exception {
        assertTrue(Topping.class.isInstance(boiledEgg));
    }
}
