package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class NoodleTest {
    private Class<?> classNoodle;

    @BeforeEach
    public void setup() throws Exception {
        classNoodle = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle");
    }

    @Test
    public void publicInterfaceNoodleTest() throws Exception {
        int classModifiers = classNoodle.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
        assertTrue((Modifier.isInterface(classModifiers)));
    }

    @Test
    public void publicAbstractGetDescriptionTest() throws Exception {
        Method getDescription = classNoodle.getDeclaredMethod("getDescription");
        int getDescriptionModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(getDescriptionModifiers));
        assertTrue(Modifier.isAbstract(getDescriptionModifiers));
    }
}
