package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MushroomTest {
    private Class<?> classMushroom;
    private Topping mushroom;

    @BeforeEach
    public void setup() throws Exception {
        classMushroom = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom");
        mushroom = new Mushroom();
    }

    @Test
    public void publicConcreteMushroomTest() throws Exception {
        int classModifiers = classMushroom.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
        assertFalse((Modifier.isInterface(classModifiers)));
    }

    @Test
    public void publicConcreteGetDescriptionTest() throws Exception {
        Method getDescription = classMushroom.getDeclaredMethod("getDescription");
        int getDescriptionModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(getDescriptionModifiers));
        assertFalse(Modifier.isAbstract(getDescriptionModifiers));
    }

    @Test
    public void publicContentsGetDescriptionTest() throws Exception {
        assertEquals("Adding Shiitake Mushroom Topping...", mushroom.getDescription());
    }

    @Test
    public void MushroomisToppingTest() throws Exception {
        assertTrue(Topping.class.isInstance(mushroom));
    }
}
