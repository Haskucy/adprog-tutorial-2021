package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FlavorTest {
    private Class<?> classFlavor;

    @BeforeEach
    public void setup() throws Exception {
        classFlavor = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor");
    }

    @Test
    public void publicInterfaceFlavorTest() throws Exception {
        int classModifiers = classFlavor.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
        assertTrue((Modifier.isInterface(classModifiers)));
    }

    @Test
    public void publicAbstractGetDescriptionTest() throws Exception {
        Method getDescription = classFlavor.getDeclaredMethod("getDescription");
        int getDescriptionModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(getDescriptionModifiers));
        assertTrue(Modifier.isAbstract(getDescriptionModifiers));
    }
}
