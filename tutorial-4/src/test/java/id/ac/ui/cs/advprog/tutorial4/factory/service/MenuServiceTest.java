package id.ac.ui.cs.advprog.tutorial4.factory.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MenuServiceTest {
    private MenuService menuService;

    @BeforeEach
    public void setup() throws Exception {
        menuService = new MenuServiceImpl();
    }

    @Test
    public void getMenuSizeFromMenuServiceImpl(){
        assertEquals(4, menuService.getMenus().size());
    }
}
