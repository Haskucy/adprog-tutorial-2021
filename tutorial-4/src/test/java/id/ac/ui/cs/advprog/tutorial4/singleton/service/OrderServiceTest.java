package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.factory.service.MenuService;
import id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderServiceTest {
    private OrderService orderService;

    @BeforeEach
    public void setup() throws Exception {
        orderService = new OrderServiceImpl();
    }

    @Test
    public void OrderDrinkTest(){
        orderService.orderADrink("Mojito");
        assertEquals("Mojito", orderService.getDrink().toString());
    }

    @Test
    public void OrderFoodTest(){
        orderService.orderAFood("Fish and Chips");

        assertEquals("Fish and Chips", orderService.getFood().toString());
    }
}

