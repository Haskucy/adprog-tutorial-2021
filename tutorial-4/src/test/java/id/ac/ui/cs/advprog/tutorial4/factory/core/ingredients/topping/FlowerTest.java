package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FlowerTest {
    private Class<?> classFlower;
    private Topping flower;

    @BeforeEach
    public void setup() throws Exception {
        classFlower = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower");
        flower = new Flower();
    }

    @Test
    public void publicConcreteFlowerTest() throws Exception {
        int classModifiers = classFlower.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
        assertFalse((Modifier.isInterface(classModifiers)));
    }

    @Test
    public void publicConcreteGetDescriptionTest() throws Exception {
        Method getDescription = classFlower.getDeclaredMethod("getDescription");
        int getDescriptionModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(getDescriptionModifiers));
        assertFalse(Modifier.isAbstract(getDescriptionModifiers));
    }

    @Test
    public void publicContentsGetDescriptionTest() throws Exception {
        assertEquals("Adding Xinqin Flower Topping...", flower.getDescription());
    }

    @Test
    public void FlowerisToppingTest() throws Exception {
        assertTrue(Topping.class.isInstance(flower));
    }
}

