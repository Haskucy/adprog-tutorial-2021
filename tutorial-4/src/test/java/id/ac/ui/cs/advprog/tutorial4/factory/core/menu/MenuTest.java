package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;


public class MenuTest {
    private Class<?> classMenu;

    @BeforeEach
    public void setup() throws Exception{
        classMenu = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void publicMenuTest() throws Exception{
        int classModifiers = classMenu.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
    }

    @Test
    public void makeFoodTest() throws Exception{
        Method makeFood = classMenu.getDeclaredMethod("makeFood");
        int makeFoodModifiers = makeFood.getModifiers();

        assertTrue(Modifier.isPrivate(makeFoodModifiers));
        assertFalse(Modifier.isAbstract(makeFoodModifiers));
        assertEquals(0, makeFood.getParameterCount());
    }

    @Test
    public void getNameTest() throws Exception{
        Method getName = classMenu.getDeclaredMethod("getName");
        int getNameModifiers = getName.getModifiers();

        assertTrue(Modifier.isPublic(getNameModifiers));
        assertFalse(Modifier.isAbstract(getNameModifiers));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void getNoodleTest() throws Exception{
        Method getNoodle = classMenu.getDeclaredMethod("getNoodle");
        int getNoodleModifiers = getNoodle.getModifiers();

        assertTrue(Modifier.isPublic(getNoodleModifiers));
        assertFalse(Modifier.isAbstract(getNoodleModifiers));
        assertEquals(0, getNoodle.getParameterCount());
    }

    @Test
    public void getMeatTest() throws Exception{
        Method getMeat = classMenu.getDeclaredMethod("getMeat");
        int getMeatModifiers = getMeat.getModifiers();

        assertTrue(Modifier.isPublic(getMeatModifiers));
        assertFalse(Modifier.isAbstract(getMeatModifiers));
        assertEquals(0, getMeat.getParameterCount());
    }

    @Test
    public void getToppingTest() throws Exception{
        Method getTopping = classMenu.getDeclaredMethod("getTopping");
        int getToppingModifiers = getTopping.getModifiers();

        assertTrue(Modifier.isPublic(getToppingModifiers));
        assertFalse(Modifier.isAbstract(getToppingModifiers));
        assertEquals(0, getTopping.getParameterCount());
    }

    @Test
    public void getFlavorTest() throws Exception{
        Method getFlavor = classMenu.getDeclaredMethod("getFlavor");
        int getFlavorModifiers = getFlavor.getModifiers();

        assertTrue(Modifier.isPublic(getFlavorModifiers));
        assertFalse(Modifier.isAbstract(getFlavorModifiers));
        assertEquals(0, getFlavor.getParameterCount());
    }


}
