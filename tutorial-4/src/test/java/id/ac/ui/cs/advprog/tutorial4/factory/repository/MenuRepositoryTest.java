package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @BeforeEach
    public void setup() throws Exception {
        menuRepository = new MenuRepository();
        menuRepository.add(new LiyuanSoba("Soba paling enak"));
        menuRepository.add(new InuzumaRamen("Ramen yang luar biasa"));
        menuRepository.add(new SnevnezhaShirataki("Shirataki yang Unik"));
    }

    @Test
    void getMenuTest(){
        assertEquals(3, menuRepository.getMenus().size());
    }

    @Test
    void addMoreTest(){
        menuRepository.add(new MondoUdon("Udon yang nikmat"));
        assertEquals(4, menuRepository.getMenus().size());
    }
}
