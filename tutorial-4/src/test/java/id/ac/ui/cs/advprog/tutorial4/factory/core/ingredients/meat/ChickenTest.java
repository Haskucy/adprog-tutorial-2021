package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChickenTest {
    private Class<?> classChicken;
    private Meat chicken;

    @BeforeEach
    public void setup() throws Exception {
        classChicken = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken");
        chicken = new Chicken();
    }

    @Test
    public void publicConcreteChikenTest() throws Exception {
        int classModifiers = classChicken.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
        assertFalse((Modifier.isInterface(classModifiers)));
    }

    @Test
    public void publicConcreteGetDescriptionTest() throws Exception {
        Method getDescription = classChicken.getDeclaredMethod("getDescription");
        int getDescriptionModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(getDescriptionModifiers));
        assertFalse(Modifier.isAbstract(getDescriptionModifiers));
    }

    @Test
    public void publicContentsGetDescriptionTest() throws Exception {
        assertEquals("Adding Wintervale Chicken Meat...", chicken.getDescription());
    }

    @Test
    public void ChickenIsMeatTest() throws Exception {
        assertTrue(Meat.class.isInstance(chicken));
    }
}

