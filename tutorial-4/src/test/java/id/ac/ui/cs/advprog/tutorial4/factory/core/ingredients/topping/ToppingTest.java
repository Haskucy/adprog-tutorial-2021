package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ToppingTest {
    private Class<?> classTopping;

    @BeforeEach
    public void setup() throws Exception {
        classTopping = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping");
    }

    @Test
    public void publicInterfaceToppingTest() throws Exception {
        int classModifiers = classTopping.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
        assertTrue((Modifier.isInterface(classModifiers)));
    }

    @Test
    public void publicAbstractGetDescriptionTest() throws Exception {
        Method getDescription = classTopping.getDeclaredMethod("getDescription");
        int getDescriptionModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(getDescriptionModifiers));
        assertTrue(Modifier.isAbstract(getDescriptionModifiers));
    }
}