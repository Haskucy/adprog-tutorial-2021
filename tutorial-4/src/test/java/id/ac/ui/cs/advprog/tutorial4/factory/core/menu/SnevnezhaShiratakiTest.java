package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiTest {
    private SnevnezhaShirataki snevnezhaShirataki;

    @BeforeEach
    public void setup() throws Exception{
        snevnezhaShirataki = new SnevnezhaShirataki("shirataki snevnezha");
    }

    @Test
    public void getNameTest() throws Exception{
        assertEquals("shirataki snevnezha",snevnezhaShirataki.getName());
    }

    @Test
    public void getNoodleTest() throws Exception{
        assertTrue((snevnezhaShirataki.getNoodle() instanceof Shirataki));
    }

    @Test
    public void getMeatTest() throws Exception{
        assertTrue((snevnezhaShirataki.getMeat() instanceof Fish));
    }

    @Test
    public void getToppingTest() throws Exception{
        assertTrue((snevnezhaShirataki.getTopping() instanceof Flower));
    }

    @Test
    public void getFlavorTest() throws Exception{
        assertTrue((snevnezhaShirataki.getFlavor() instanceof Umami));
    }
}
