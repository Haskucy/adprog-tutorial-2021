package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UdonTest {
    private Class<?> classUdon;
    private Noodle udon;

    @BeforeEach
    public void setup() throws Exception {
        classUdon = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon");
        udon = new Udon();
    }

    @Test
    public void publicConcreteUdonTest() throws Exception {
        int classModifiers = classUdon.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
        assertFalse((Modifier.isInterface(classModifiers)));
    }

    @Test
    public void publicConcreteGetDescriptionTest() throws Exception {
        Method getDescription = classUdon.getDeclaredMethod("getDescription");
        int getDescriptionModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(getDescriptionModifiers));
        assertFalse(Modifier.isAbstract(getDescriptionModifiers));
    }

    @Test
    public void publicContentsGetDescriptionTest() throws Exception {
        assertEquals("Adding Mondo Udon Noodles...", udon.getDescription());
    }

    @Test
    public void UdonIsNoodleTest() throws Exception {
        assertTrue(Noodle.class.isInstance(udon));
    }
}

