package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MeatTest {
    private Class<?> classMeat;

    @BeforeEach
    public void setup() throws Exception {
        classMeat = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat");
    }

    @Test
    public void publicInterfaceMeatTest() throws Exception {
        int classModifiers = classMeat.getModifiers();

        assertTrue((Modifier.isPublic(classModifiers)));
        assertTrue((Modifier.isInterface(classModifiers)));
    }

    @Test
    public void publicAbstractGetDescriptionTest() throws Exception {
        Method getDescription = classMeat.getDeclaredMethod("getDescription");
        int getDescriptionModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(getDescriptionModifiers));
        assertTrue(Modifier.isAbstract(getDescriptionModifiers));
    }
}
