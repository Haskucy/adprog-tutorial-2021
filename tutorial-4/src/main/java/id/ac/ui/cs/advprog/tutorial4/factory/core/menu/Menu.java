package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menufactory.IngredientsAbstractFactory;

public class Menu {
    private String name;
    private Noodle noodle;
    private Meat meat;
    private Topping topping;
    private Flavor flavor;
    protected IngredientsAbstractFactory ingredientsAbstractFactory;

    //To Do : Complete Me
    //Silahkan tambahkan parameter jika dibutuhkan
    public Menu(String name,IngredientsAbstractFactory type){
        this.name = name;
        this.ingredientsAbstractFactory = type;
        makeFood();
    }

    private void makeFood() {
        noodle = ingredientsAbstractFactory.createNoodle();
        meat = ingredientsAbstractFactory.createMeat();
        topping = ingredientsAbstractFactory.createTopping();
        flavor = ingredientsAbstractFactory.createFlavor();

    }

    public String getName() {
        return name;
    }

    public Noodle getNoodle() {
        return noodle;
    }

    public Meat getMeat() {
        return meat;
    }

    public Topping getTopping() {
        return topping;
    }

    public Flavor getFlavor() {
        return flavor;
    }
}