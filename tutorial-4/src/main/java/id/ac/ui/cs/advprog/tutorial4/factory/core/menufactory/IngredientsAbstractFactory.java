package id.ac.ui.cs.advprog.tutorial4.factory.core.menufactory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public interface IngredientsAbstractFactory {

    public Noodle createNoodle();
    public Meat createMeat();
    public Topping createTopping();
    public Flavor createFlavor();

}
